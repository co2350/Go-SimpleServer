package ss

import (
	"errors"
	"fmt"
	"log"
	"net"
)

func init() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
}

// JobQueue : 工作管道
var JobQueue chan Job

// workerPool ： 执行线程池
var workerPool chan chan Job
var server Server

// MaxWORKERS ： 执行线程数量
var MaxWORKERS int

// MaxQUEUE ： 工作队列数量
var MaxQUEUE int

// Start : 调起一个线程等待执行管道被推入Job并执行
func (w Worker) Start() {
	go func() {
		for {
			//将空闲的工作线程推入工作池
			w.WorkerPool <- w.JobChannel

			select {
			//等待工作线程被推入工作
			case job := <-w.JobChannel:
				err := job.dealWithData()
				if err != nil {
					log.Println(err)
					server.SendMessage(job.rmAddr, 0, []byte{})
				}
				server.SendMessage(job.rmAddr, 1, []byte{})
			case <-w.QuitChan:
				return
			}
		}
	}()
}

// Stop ： 关闭执行线程
func (w Worker) Stop() {
	go func() {
		w.QuitChan <- true
	}()
}

// Create : 创建SimpleServer
// 参数：rule 入站规则，255为flag值,254为前后规则分界。254不可存在一个以上
// maxWorker 线程数量推荐为核心数*12
// maxQueue 工作队列树建议不超过线程数量*40
func Create(port string, rule []byte, maxWorker, maxQueue int) (Server, error) {
	//解析出UDP地址
	addr, err := net.ResolveUDPAddr("udp", ":"+port)
	if err != nil {
		log.Println("错误：", err)
		return Server{nil, nil, nil}, err
	}
	//创建UDP连接
	con, err := net.ListenUDP("udp", addr)
	if err != nil {
		log.Println("错误：", err)
		return Server{nil, nil, nil}, err
	}

	JobQueue = make(chan Job, maxQueue)
	MaxQUEUE = maxQueue
	MaxWORKERS = maxWorker
	server = Server{con, make(map[byte]func(*net.UDPAddr, []byte)), rule}
	return server, nil
}

// Start : 开始监听端口。
// 并且等待Job推入工作队列后再把Job推入执行管道
func (server Server) Start() {
	workerPool := make(chan chan Job)

	for i := 0; i < MaxWORKERS; i++ {
		worker := newWorker(i, workerPool)
		worker.Start()
	}

	go func() {
		for {
			select {
			//等待工作队列被推入数据
			case job := <-JobQueue:
				go func(job Job) {
					//从工作池中推出一个工作线程
					worker := <-workerPool
					//将Job推入工作线程
					worker <- job
				}(job)
			}
		}
	}()

	go server.startLinsten()

}

// StartLinsten : 开Go程进行轮询
// 接受消息之后打包成Job后推入工作队列
func (server Server) startLinsten() {
	recvBuff := make([]byte, 1024)
	for {
		//等待远端消息
		rn, rmAddr, err := server.conn.ReadFromUDP(recvBuff)
		if err != nil {
			log.Println("错误：", err)
			return
		}

		//推入工作队列
		work := Job{rmAddr, recvBuff[:rn]}
		JobQueue <- work
	}
}

// DealWithData : 处理入站请求
func (job Job) dealWithData() error {
	//判断数据正确性
	flag, middle, err := server.jugementData(job.byMsg)
	if err != nil {
		return errors.New(fmt.Sprintln("错误：", err))
	}
	//判断并执行委托的事件
	process, ok := server.param[flag]
	if ok {
		if middle == -1 {
			go process(job.rmAddr, job.byMsg[len(server.rule):])
		} else {
			go process(job.rmAddr, job.byMsg[middle:len(job.byMsg)-(len(server.rule)-middle)+1])
		}
	} else {
		return errors.New(fmt.Sprintln("入站请求flag不在监听中，flag为"+string(flag)+"\n数据为：") + fmt.Sprintln(job.byMsg))
	}
	return nil
}

// AddLinstener : 给Server对象添加监听
func (server Server) AddLinstener(flag byte, event func(*net.UDPAddr, []byte)) error {
	var err error
	_, ok := server.param[flag]
	//确认param中是否已经包含flag
	if ok {
		err = errors.New("已存在此监听")
	} else {
		server.param[flag] = event
		err = nil
	}
	return err
}

// SendMessage : 发送消息给远端
func (server Server) SendMessage(rmAddr *net.UDPAddr, flag byte, byMsg []byte) {
	message := make([]byte, 0)
	message = append(message, flag, 0, flag, 0)
	message = append(message, byMsg...)
	server.conn.WriteToUDP(message, rmAddr)
}
