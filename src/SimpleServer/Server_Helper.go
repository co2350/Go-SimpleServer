package ss

import (
	"errors"
	"fmt"
)

// jugementData : 判断数据正确性
func (server Server) jugementData(byMsg []byte) (byte, int, error) {
	var flag byte
	mirror := 0
	middle := -1
	for i, b := range server.rule {
		i += mirror
		switch {
		case b == 255:
			if flag == 0 {
				flag = byMsg[i]
			} else if flag != byMsg[i] {
				return 0, 0, errors.New("数据 " + fmt.Sprintln(byMsg))
			}
		case b == 254:
			if mirror == 0 {
				middle = i
				mirror = len(byMsg) - len(server.rule)
			} else {
				return 0, 0, errors.New("数据 " + fmt.Sprintln(byMsg))
			}
		case b != byMsg[i]:
			return 0, 0, errors.New("数据 " + fmt.Sprintln(byMsg))
		}
	}
	return flag, middle, nil
}
