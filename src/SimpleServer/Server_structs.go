package ss

import (
	"net"
)

// Server : 保存了UDP连接和委托的处理
type Server struct {
	conn  *net.UDPConn
	param map[byte]func(*net.UDPAddr, []byte)
	rule  []byte
}

// Job ： 工作数据
type Job struct {
	rmAddr *net.UDPAddr
	byMsg  []byte
}

// Worker ： 工作线程
type Worker struct {
	ID         int
	JobChannel chan Job
	WorkerPool chan chan Job
	QuitChan   chan bool
}

// newWorker ： 实例化一个工作线程
func newWorker(id int, workerPool chan chan Job) Worker {
	worker := Worker{
		ID:         id,
		JobChannel: make(chan Job),
		WorkerPool: workerPool,
		QuitChan:   make(chan bool)}
	return worker
}
